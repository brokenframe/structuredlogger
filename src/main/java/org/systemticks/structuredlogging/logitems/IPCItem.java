package org.systemticks.structuredlogging.logitems;

import java.util.ArrayList;
import java.util.List;

import org.systemticks.structuredlogging.ImmutableTuple;

public class IPCItem {

	String source;
	String destination;
	String type;
	String summary;
	
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}

	MethodTracing method;
    List<ImmutableTuple<String, String>> taggedValues = new ArrayList<>();
    
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public MethodTracing getMethod() {
		return method;
	}
	public void setMethod(MethodTracing method) {
		this.method = method;
	}
	public List<ImmutableTuple<String, String>> getTaggedValues() {
		return taggedValues;
	}

    public void addTaggedValue(String key, String value) {
        taggedValues.add(new ImmutableTuple<String, String>(key, value));
    }
    
	
}
