package org.systemticks.structuredlogging.logitems;

import java.util.ArrayList;
import java.util.List;

import org.systemticks.structuredlogging.ImmutableTuple;

/**
 * Created by tomo3867 on 06.08.2018.
 */

public class SLogItem {

    long timestamp;
    String channel;
    String message;

    int proceddId;
    int threadId;

    List<ImmutableTuple<String, String>> taggedValues = new ArrayList<>();

    public int getProceddId() {
        return proceddId;
    }

    public void setProceddId(int proceddId) {
        this.proceddId = proceddId;
    }

    public int getThreadId() {
        return threadId;
    }

    public void setThreadId(int threadId) {
        this.threadId = threadId;
    }

    MethodTracing method;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MethodTracing getMethod() {
        return method;
    }

    public void setMethod(MethodTracing method) {
        this.method = method;
    }

    public void addTaggedValue(String key, String value) {
        taggedValues.add(new ImmutableTuple<String, String>(key, value));
    }

    public List<ImmutableTuple<String, String>> getTaggedValues() {
        return taggedValues;
    }
}
