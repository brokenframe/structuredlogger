package org.systemticks.structuredlogging.logitems;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import org.systemticks.structuredlogging.ImmutableTuple;

public class MethodTracingSerializer implements JsonSerializer<MethodTracing> {

    @Override
    public JsonElement serialize(MethodTracing src, Type typeOfSrc, JsonSerializationContext context) {

        final JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("type", src.getType());
        jsonObject.addProperty("name", src.getMethod());
        jsonObject.addProperty("class", src.getClazz());
        jsonObject.addProperty("serial", src.getSerial());

        if(src.hasParams()) {
            final JsonObject params = new JsonObject();
            for(ImmutableTuple<String, Object> p: src.getParams()) {
            	try 
            	{
                    final JsonElement value = context.serialize(p.getValue());
                    params.add(p.getKey(), value);            		
            	}
            	catch(SecurityException e) {
                	params.addProperty(p.getKey(), p.getValue().toString());            		
            	}
            }
            jsonObject.add("params", params);
        }

        if(src.getCallerClass() != null && src.getCallerMethod() != null)
        {
            final JsonObject caller = new JsonObject();
            caller.addProperty("method", src.getCallerMethod());
            caller.addProperty("class", src.getCallerClass());
            jsonObject.add("caller", caller);
        }

        return jsonObject;
    }

}
