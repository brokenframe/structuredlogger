package org.systemticks.structuredlogging.logitems;

import java.lang.reflect.Type;

import org.systemticks.structuredlogging.ImmutableTuple;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class IPCItemSerializer implements JsonSerializer<IPCItem> {

	@Override
	public JsonElement serialize(IPCItem src, Type typeOfSrc, JsonSerializationContext context) {

		final JsonObject jsonObject = new JsonObject();

		jsonObject.addProperty("summary", src.getSummary());

		final JsonObject value = new JsonObject();

		final JsonElement method = context.serialize(src.getMethod());
		value.add("method", method);

		final JsonObject meta = new JsonObject();
		for (ImmutableTuple<String, String> pair : src.getTaggedValues()) {
			meta.addProperty(pair.getKey(), pair.getValue());
		}
		value.add("metadata", meta);
		
		jsonObject.add("value", value);

		final JsonObject edge = new JsonObject();
		edge.addProperty("source", src.getSource());
		edge.addProperty("destination", src.getDestination());
		edge.addProperty("type", src.getType());

		jsonObject.add("edge", edge);

		return jsonObject;

	}

}
