package org.systemticks.structuredlogging.logitems;

import java.util.ArrayList;
import java.util.List;

import org.systemticks.structuredlogging.ImmutableTuple;

public class MethodTracing {

    private String type;
    private String method;
    private String clazz;
    private String callerMethod;
    private List<ImmutableTuple<String, Object>> params;

    public String getCallerMethod() {
        return callerMethod;
    }

    public void setCallerMethod(String callerMethod) {
        this.callerMethod = callerMethod;
    }

    public String getCallerClass() {
        return callerClass;
    }

    public void setCallerClass(String callerClass) {
        this.callerClass = callerClass;
    }

    private String callerClass;

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    private int serial;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public void addParam(String name, Object value) {
        if(params == null)
        {
            params = new ArrayList<>();
        }
        params.add(new ImmutableTuple<String, Object>(name, value));
    }

    public boolean hasParams() {
        return params != null && params.size() > 0;
    }

    public List<ImmutableTuple<String, Object>> getParams() {
        return params;
    }

}
