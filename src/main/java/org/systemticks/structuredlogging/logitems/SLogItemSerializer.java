package org.systemticks.structuredlogging.logitems;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import org.systemticks.structuredlogging.ImmutableTuple;

/**
 * Created by tomo3867 on 06.08.2018.
 */

public class SLogItemSerializer implements JsonSerializer<SLogItem> {

    @Override
    public JsonElement serialize(SLogItem src, Type typeOfSrc, JsonSerializationContext context) {

        final JsonObject jsonObject = new JsonObject();

//        jsonObject.addProperty("uptime", Long.toString(src.getTimestamp()));
//        jsonObject.addProperty("channel", src.getChannel());
        jsonObject.addProperty("summary", src.getMessage());

//        final JsonObject value = new JsonObject();
//        value.addProperty("pid", src.getProceddId());
//        value.addProperty("tid", src.getThreadId());

        for(ImmutableTuple<String, String> pair: src.getTaggedValues()) {
            jsonObject.addProperty(pair.getKey(), pair.getValue());
        }

        final JsonElement method = context.serialize(src.getMethod());
        jsonObject.add("method", method);

//        jsonObject.add("value", value);

        return jsonObject;
    }
}
