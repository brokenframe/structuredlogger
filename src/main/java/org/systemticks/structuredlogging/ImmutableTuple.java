package org.systemticks.structuredlogging;

public class ImmutableTuple<T, S> {

    private final T key;
    private final S value;

    public ImmutableTuple(T _key, S _value)
    {
        this.key = _key;
        this.value = _value;
    }

    public T getKey() {
        return key;
    }

    public S getValue() {
        return value;
    }
}
