package org.systemticks.structuredlogging;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.systemticks.structuredlogging.logitems.IPCItem;
import org.systemticks.structuredlogging.logitems.IPCItemSerializer;
import org.systemticks.structuredlogging.logitems.MethodTracing;
import org.systemticks.structuredlogging.logitems.MethodTracingSerializer;
import org.systemticks.structuredlogging.logitems.SLogItem;
import org.systemticks.structuredlogging.logitems.SLogItemSerializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SLog {

	static Gson methodSerializer = null;

    private static final Object serializerLock = new Object();
	
	private static final Stack<ImmutableTuple<Integer, String>> stack = new Stack<>();
	private static int serial = 0;
	private static final int STACK_ITEM_CALLER = 1;

	static Logger log = LoggerFactory.getLogger("MethodLogging");
	
	static Logger ipclogger = LoggerFactory.getLogger("IPCLogging");
	

	private static List<ImmutableTuple<Type, Object>> serializers = new ArrayList<>(
			Arrays.asList(new ImmutableTuple<Type, Object>(SLogItem.class, new SLogItemSerializer()),
					new ImmutableTuple<>(IPCItem.class, new IPCItemSerializer()),
					new ImmutableTuple<>(MethodTracing.class, new MethodTracingSerializer())));

	public static void registerSerializer(Type type, Object typeAdapter) {
		serializers.add(new ImmutableTuple<Type, Object>(type, typeAdapter));
		synchronized (serializerLock) {
			methodSerializer = null;
		}			
	}

	private static String toJson(SLogItem item) {

		synchronized (serializerLock) {

			if (methodSerializer == null) {
				GsonBuilder builder = new GsonBuilder();
				for (ImmutableTuple<Type, Object> s : serializers) {
					builder.registerTypeAdapter(s.getKey(), s.getValue());
				}
				methodSerializer = builder.create();
			}

			return methodSerializer.toJson(item);
		}

	}

	private static String toJson(IPCItem item) {

		synchronized (serializerLock) {

			if (methodSerializer == null) {
				GsonBuilder builder = new GsonBuilder();
				for (ImmutableTuple<Type, Object> s : serializers) {
					builder.registerTypeAdapter(s.getKey(), s.getValue());
				}
				methodSerializer = builder.create();
			}

			return methodSerializer.toJson(item);
		}

	}
	
	
	@SuppressWarnings("unchecked")
	public static ImmutableTuple<String, String>[] createTaggedValues(String...taggedValues) {
					
		if(taggedValues.length % 2 != 0) return null;

		ImmutableTuple<String, String>[] tV = new ImmutableTuple[taggedValues.length/2];
		
		for(int i=0; i<taggedValues.length; i+=2) {
			tV[i/2] = new ImmutableTuple<String, String>(taggedValues[i], taggedValues[i+1]);
		}
		
		return tV;
	}	
	
	@SuppressWarnings("unchecked")
	public static ImmutableTuple<String, Object>[] createParams(Object...params) {
					
		if(params.length % 2 != 0) return null;

		ImmutableTuple<String, Object>[] p = new ImmutableTuple[params.length/2];
		
		for(int i=0; i<params.length; i+=2) {
			p[i/2] = new ImmutableTuple<String, Object>((String)params[i], params[i+1]);
		}
		
		return p;
	}	

	
	public static void logIpcRequest(String message, String source, String destination, 
			ImmutableTuple<String, Object>[] params, ImmutableTuple<String, String>[] taggedValues) {
		logIpcCall(message, source, destination, params, taggedValues, "request");
	}

	public static void logIpcResponse(String message, String source, String destination, 
			ImmutableTuple<String, Object>[] params, ImmutableTuple<String, String>[] taggedValues) {
		logIpcCall(message, source, destination, params, taggedValues, "response");
	}
		
	public static void logIpcCall(String message, String source, String destination, 
			ImmutableTuple<String, Object>[] params, ImmutableTuple<String, String>[] taggedValues, String direction) {

		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		String methodname = stacktrace[STACK_ITEM_CALLER].getMethodName();
		String classname = stacktrace[STACK_ITEM_CALLER].getClassName();
		
		IPCItem item = new IPCItem();
		item.setSource(source);
		item.setDestination(destination);
		item.setType(direction);
		
		item.setSummary(message);
		
		MethodTracing m = new MethodTracing();
		m.setMethod(methodname);
		m.setClazz(classname);
		item.setMethod(m);

		if (params != null) {
			for (ImmutableTuple<String, Object> p : params) {
				m.addParam(p.getKey(), p.getValue());
			}
		}			
	
		if (taggedValues != null) {
			for (ImmutableTuple<String, String> p : taggedValues) {
				item.addTaggedValue(p.getKey(), p.getValue());
			}
		}
		
		ipclogger.info(toJson(item));			
		
		
	}
	
	/**
	 * Logs the entry of a method call. Method name and class name will be determined dynamically, based on the stack trace 
	 * @param params A list of parameters you want to log. Should be created with SLog.createParams
	 * @param taggedValues A list of tagged values you want to add to the trace. Should be created with SLog.createTaggedValues
	 */
	public static void logMethodEnter(ImmutableTuple<String, Object>[] params, ImmutableTuple<String, String>[] taggedValues) {
		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		logMethodEnter(stacktrace[STACK_ITEM_CALLER].getClassName(), stacktrace[STACK_ITEM_CALLER].getMethodName(), params, taggedValues);		
	}
	
	/**
	 * Logs the entry of a method call. Method name and class name will be determined dynamically, based on the stack trace 
	 * @param taggedValues
	 * @param taggedValues A list of tagged values you want to add to the trace. Should be created with SLog.createTaggedValues
	 */
	public static void logMethodEnter(ImmutableTuple<String, String>[] taggedValues) {
		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		logMethodEnter(stacktrace[STACK_ITEM_CALLER].getClassName(), stacktrace[STACK_ITEM_CALLER].getMethodName(), taggedValues);
	}

	
	/**
	 * Logs the entry of a method call. 
	 * @param classname The class name
	 * @param methodname The method name
	 * @param taggedValues A list of tagged values you want to add to the trace. Should be created with SLog.createTaggedValues
	 */
	public static void logMethodEnter(String classname, String methodname, 	ImmutableTuple<String, String>[] taggedValues) {
		logMethodEnter(classname, methodname, null, taggedValues);
	}
	
	/**
	 * Logs the entry of a method call. 
	 * @param classname The class name
	 * @param methodname The method name
	 * @param params A list of parameters you want to log. Should be created with SLog.createParams
	 * @param taggedValues A list of tagged values you want to add to the trace. Should be created with SLog.createTaggedValues
	 */
	public static void logMethodEnter(String classname, String methodname, ImmutableTuple<String, Object>[] params,
			ImmutableTuple<String, String>[] taggedValues) {

		MethodTracing m = new MethodTracing();
		m.setMethod(methodname);
		m.setClazz(classname);

		if (params != null) {
			for (ImmutableTuple<String, Object> p : params) {
				m.addParam(p.getKey(), p.getValue());
			}
		}

		if (!stack.isEmpty()) {
			ImmutableTuple<Integer, String> caller = stack.peek();
			String[] tokens = caller.getValue().split("\\.");
			m.setCallerClass(tokens[0]);
			m.setCallerMethod(tokens[1]);
		}

		serial += 1;
		m.setSerial(serial);
		stack.push(new ImmutableTuple<Integer, String>(serial, classname + "." + methodname));

		SLogItem item = new SLogItem();
		item.setMethod(m);
		item.addTaggedValue("method_type", "enter");

		if (taggedValues != null) {
			for (ImmutableTuple<String, String> p : taggedValues) {
				item.addTaggedValue(p.getKey(), p.getValue());
			}
		}

		log.info(toJson(item));
	}

	public static void logMethodExit(String classname, String methodname,  ImmutableTuple<String, String>[] taggedValues) {
		logMethodExit(classname, methodname, null, taggedValues);
	}
	
	public static void logMethodExit(String classname, String methodname, Object result,
			ImmutableTuple<String, String>[] taggedValues) {
		MethodTracing m = new MethodTracing();
		m.setMethod(methodname);
		m.setClazz(classname);
		m.setSerial(stack.pop().getKey());

		if (result != null) {
			m.addParam("result", result);
		}

		SLogItem item = new SLogItem();
		item.setMethod(m);
		item.addTaggedValue("method_type", "exit");

		if (taggedValues != null) {
			for (ImmutableTuple<String, String> p : taggedValues) {
				item.addTaggedValue(p.getKey(), p.getValue());
			}
		}

		log.info(toJson(item));
	}


}
