package org.systemticks.structuredlogging;

import java.util.Date;

public class CustomTestType2 {

	private String aString;
	private int anInt;
	private Date aDate;
	private CustomTestType custom;
	
	public CustomTestType2(String aString, int anInt, Date aDate) {
		super();
		this.aString = aString;
		this.anInt = anInt;
		this.aDate = aDate;
		this.custom = new CustomTestType(aString, anInt, aDate);
	}

	public String getaString() {
		return aString;
	}

	public int getAnInt() {
		return anInt;
	}

	public Date getaDate() {
		return aDate;
	}
	
	public CustomTestType getCustomTestType() 
	{
		return custom;
	}
	
	
	
}
