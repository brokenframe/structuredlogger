package org.systemticks.structuredlogging;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConcurrencyTest {

	@Test(expected = Test.None.class /* no exception expected */)
	public void test() {
		SLog.registerSerializer(CustomTestType.class, new CustomTestTypeSerialzer());
		SLog.logMethodEnter("ConcurrencyTest", "test", null, null);
	}

}
