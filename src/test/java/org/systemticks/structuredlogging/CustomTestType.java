package org.systemticks.structuredlogging;

import java.util.Date;

public class CustomTestType {

	private String aString;
	private int anInt;
	private Date aDate;
	
	public CustomTestType(String aString, int anInt, Date aDate) {
		super();
		this.aString = aString;
		this.anInt = anInt;
		this.aDate = aDate;
	}

	public String getaString() {
		return aString;
	}

	public int getAnInt() {
		return anInt;
	}

	public Date getaDate() {
		return aDate;
	}
	
	
	
}
