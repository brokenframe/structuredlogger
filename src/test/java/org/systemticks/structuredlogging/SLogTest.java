package org.systemticks.structuredlogging;

import java.util.Date;

import org.junit.Test;

public class SLogTest {

	@Test
	public void testLogMethodEnterAutoInfo() {		
		SLog.logMethodEnter(null);
	}

	@Test
	public void testLogMethodEnterAutoInfoParams() {		
		SLog.logMethodEnter(SLog.createParams("a",1,"b", true, "c", 4711) ,null);
	}

	
	@Test
	public void testLogMethodEnter() {
		
		SLog.logMethodEnter("LogUtilTest", "testLogMethodEnter", null, null);
		SLog.logMethodExit("LogUtilTest", "testLogMethodEnter", true, null);
		
	}

	@Test
	public void testLogMethodEnterWithParams() {
		
		ImmutableTuple<String, Object>[] params = SLog.createParams("aString","text", "anInt", 4711, "aDouble", 17.34, "aDate", new Date());
		
		SLog.logMethodEnter("LogUtilTest", "testLogMethodEnterWithParams", params , null);
		SLog.logMethodExit("LogUtilTest", "testLogMethodEnterWithParams", false, null);

	}

	@Test
	public void testCustomType()
	{
		SLog.registerSerializer(CustomTestType.class, new CustomTestTypeSerialzer());
		
		ImmutableTuple<String, Object>[] params = SLog.createParams("custom", new CustomTestType("bla", 4711, new Date())) ;
		SLog.logMethodEnter("LogUtilTest", "testCustomType", params, null);
		SLog.logMethodExit("LogUtilTest", "testCustomType", null , null);		
	}

	@Test
	public void testCustomType2()
	{		
		ImmutableTuple<String, Object>[] params = SLog.createParams("custom", new CustomTestType2("bla", 4711, new Date())) ;
		SLog.logMethodEnter("LogUtilTest", "testCustomType2", params, null);
		SLog.logMethodExit("LogUtilTest", "testCustomType2", null , null);		
	}

	@Test
	public void testIPCRequest() {

		ImmutableTuple<String, Object>[] params = SLog.createParams("custom", new CustomTestType2("bla", 4711, new Date())) ;

		SLog.logIpcRequest("testIPCRequest", "client", "service", params, null);
		
	}
	
}
