package org.systemticks.structuredlogging;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class CustomTestTypeSerialzer implements JsonSerializer<CustomTestType> {

	@Override
	public JsonElement serialize(CustomTestType src, Type typeOfSrc, JsonSerializationContext context) {
		
        final JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("a", src.getaString());
        jsonObject.addProperty("b", src.getAnInt());
        jsonObject.addProperty("c", src.getaDate().getTime());
        
        return jsonObject;
		
	}
}
